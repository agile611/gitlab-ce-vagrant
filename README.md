[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)

# Agile611 Gitlab-ce Environment

This is a [Vagrant](https://www.vagrantup.com/) Environment for a Gitlab. 

It provides the [Message Transfer Agent (MTA)](https://en.wikipedia.org/wiki/Message_transfer_agent) and the [Mail Delivery Agent (MDA)](https://en.wikipedia.org/wiki/Mail_delivery_agent).

This lets you easily test your application code against a real sandboxed Gitlab Server.

It uses the following software stack:

* [Postfix](http://www.postfix.org/) to the handle mail storage, reception, and transmission using the [Simple Mail Transfer Protocol (SMTP)](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol).
  * Also show how to configure Postfix in Satellite mode to relay emails to the Postfix server.
  * Also show how to configure nullmailer to relay emails to the Postfix server.
* [Dovecot](http://www.dovecot.org/) to access the mail storage using the [Internet Message Access Protocol (IMAP)](https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol).
* Dovecot for providing User Authentication to Postfix ([SMTP AUTH](https://en.wikipedia.org/wiki/SMTP_Authentication)) through the [Simple Authentication and Security Layer (SASL)](https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer).
* [Gitlab CE](http://www.gitlab.com/) GitLab Inc. is the open-core company that provides GitLab, the DevOps platform that combines the ability to develop, secure, and operate software in a single application.

# Usage

Build and install the [Ubuntu Base Box](https://github.com/rgl/ubuntu-vagrant).

Run `vagrant up` to configure the `gitlab.example.com` mail server environment.

Configure your system `/etc/hosts` file with the `example.com` and `gitlab.example.com` domains:

    192.168.56.1 example.com
    192.168.56.1 gitlab.example.com

Configure your `/etc/hosts` running ....

```shell
youruser@localEnvironment$ echo '192.168.56.1 gitlab.example.com' | sudo tee -a /etc/hosts
```

* Start the environment as follows:

```shell
youruser@localEnvironment$ vagrant up 
youruser@localEnvironment$ vagrant ssh
```

## Support

This tutorial is released into the public domain by Agile611 under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) and is likewise released into the public domain.

Please contact Agile611 for further details.

* Agile611
* Laureà Miró 309
* 08950 Esplugues de Llobregat (Barcelona)